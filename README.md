# JondellCorpAccounts

The project includes pages to upload account files and display account balances for the available accounts.

Please note that the frontend project is a work in progress.

## Backend API

Backend API for the project is deployed at `https://accountmanagementapideployment.azurewebsites.net/`. 

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

