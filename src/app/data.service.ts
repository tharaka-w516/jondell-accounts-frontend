import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private REST_API_SERVER = "https://accountmanagementapideployment.azurewebsites.net/api/accounts";

  constructor(private httpClient: HttpClient) { }

  public sendGetRequest() {
    return this.httpClient.get(this.REST_API_SERVER);
  }

  public getAccountBalances(month: number, year: number) {
    return this.httpClient.get(`${this.REST_API_SERVER}/balance/month/${month}/year/${year}`);
  }
}
