import { Component, ElementRef, OnInit, VERSION, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDatepicker } from '@angular/material/datepicker';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  name = 'Angular ' + VERSION.major;
  dataFile: any;
  date = new FormControl();
  
  @ViewChild('fileInput') fileInput!: ElementRef;
  fileAttr = 'Choose File';

  uploadFileEvt(accFile: any) {
    if (accFile.target.files && accFile.target.files[0]) {
      this.fileAttr = '';
      Array.from(accFile.target.files).forEach((file: any) => {
        this.fileAttr += file.name;
      });

      let reader = new FileReader();
      reader.onload = (e: any) => {
        console.log(e.target.result);
      };
      reader.readAsDataURL(accFile.target.files[0]);

      this.fileInput.nativeElement.value = "";
    } else {
      this.fileAttr = 'Choose File';
    }
  }

  get datePlaceHolder(): string {
    const format = ['MM', '/', 'YYYY'];

    if (this.date.value) {
      //set month
      if (this.date.value[0]) {
        // append zero in front of month values which are less than 10
        const month = this.date.value[0] < 10 ? `0${this.date.value[0]}` : this.date.value[0];
        format[0] = month;
      }
      // set year
      if (this.date.value[1]) {
        format[2] = this.date.value[1];
      }
    }
    return format.join('');
  }

  onYearSelection(d: Date) {
    const value: null | [number, number] = this.date.value;
    const year = d.getFullYear();

    if (value) {
      if (value[1] !== year) {
        value[1] = year;
        this.date.setValue(value);
      }
      return;
    } else {
      const value = [undefined, year];
      this.date.setValue(value);
    }
  }

  onMonthSelection(selectedDate: Date, datePicker: MatDatepicker<Date>) {
    const value: null | [number, number] = this.date.value;
    const month = selectedDate.getMonth() + 1;

    if (value) {
      if (value[0] !== month) {
        value[0] = month;
        this.date.setValue(value);
      }
    } else {
      const monthFormat = [month, undefined];
      this.date.setValue(monthFormat);
    }

    datePicker.close();
  }

  // constructor() { }

  ngOnInit(): void {
  }

}
